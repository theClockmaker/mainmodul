/**
 * The description of the module.
 */

#ifndef THWildmyMenue_H
#define THWildmyMenue_H

#include <boost/shared_ptr.hpp>
#include <alcommon/almodule.h>
#include <string>

#include <alproxies/almemoryproxy.h>
#include <alcommon/alproxy.h>
#include <alproxies/almotionproxy.h>
#include <alproxies/altexttospeechproxy.h>
#include <althread/almutex.h>

namespace AL
{
  class ALBroker;
}

class THWildmyMenue : public AL::ALModule
{
  public:

    THWildmyMenue(boost::shared_ptr<AL::ALBroker> broker, const std::string& name);

    virtual ~THWildmyMenue();

    /** Overloading ALModule::init().
    * This is called right after the module has been loaded
    */
    virtual void init();

    /**
    * This method will be called every time the event RightBumperPressed is raised.
    */
    void onRightBumperPressed();

    /**
    * This method will be called every time the event LeftBumperPressed is raised.
    */
    void onLeftBumperPressed();
    /**
      * this method will be called evry time the gamemodule is finished
    */
    void onGameFinished();

    private:


    //define some variables
    AL::ALMemoryProxy fMemoryProxy;
    AL::ALTextToSpeechProxy fTtsProxy;
    AL::ALMotionProxy motion;
    boost::shared_ptr<AL::ALMutex> fCallbackMutex;
    float fState;
};

#endif  // THWildmyMenue_H
