/**
 * The implementation of the module.
 */

#include "THWildmyMenue.h"

#include <alvalue/alvalue.h>
#include <alcommon/albroker.h>
#include <qi/log.hpp>
#include <althread/alcriticalsection.h>


/**
 * The constructor which sets up the module with its description and methods.
 */
THWildmyMenue::THWildmyMenue(
  boost::shared_ptr<AL::ALBroker> broker,
  const std::string& name): AL::ALModule(broker, name),
    fCallbackMutex(AL::ALMutex::createALMutex())
{
  setModuleDescription("This module presents how to subscribe to a simple event (here RightBumperPressed and LeftBumperPressed) and use a callback method.");

  // The callback methods to use when a bumper is pressed.
  functionName("onRightBumperPressed", getName(), "Method called when the right bumper is pressed. Makes a LED animation.");
  BIND_METHOD(THWildmyMenue::onRightBumperPressed);

  functionName("onLeftBumperPressed", getName(), "Method called when the left bumper is pressed. Makes a LED animation.");
  BIND_METHOD(THWildmyMenue::onLeftBumperPressed);

  functionName("onGameFinished", getName(), "Method called when game finished");
  BIND_METHOD(THWildmyMenue::onGameFinished);

  functionName("init", getName(), "Method called to start games");
  BIND_METHOD(THWildmyMenue::init);

}

/**
 * The method being called when unloading the module.
 */
THWildmyMenue::~THWildmyMenue() {
    // unsubscribe the callback methods
  fMemoryProxy.unsubscribeToEvent("onRightBumperPressed", getName());
  fMemoryProxy.unsubscribeToEvent("onLeftBumperPressed", getName());
}

/**
  * The method being called when loading the module.
  */
void THWildmyMenue::init() {
  try {

    // create a proxy to text to speech
    fTtsProxy = AL::ALTextToSpeechProxy(getParentBroker());
    // create a proxy to the memory module
    fMemoryProxy = AL::ALMemoryProxy(getParentBroker());

    //random number seed
     std::srand((unsigned)std::time(0));

    /** Subscribe to events
    * Arguments:
    * - name of the event
    * - name of the module to be called for the callback
    * - name of the bound method to be called on event
    */
    fMemoryProxy.subscribeToEvent("RightBumperPressed", getName(),
                                  "onRightBumperPressed");
    fMemoryProxy.subscribeToEvent("LeftBumperPressed", getName(),
                                  "onLeftBumperPressed");
    fMemoryProxy.subscribeToEvent("GameFinished",getName(),"onGameFinished");
    fTtsProxy.say("Hallo! Hast du lust auf ein Spielchen? Drücke Linken Bumper für Tabu und Rechten Bumper für Pantomieme");
  }
  catch (const AL::ALError& e) {
    qiLogError("THWildPantomime") << e.what() << std::endl;
  }
}

/**
 * The callback method being called when the RightBumperPressed event occured.
 */
void THWildmyMenue::onRightBumperPressed() {
  AL::ALMotionProxy motion;
  motion.setStiffnesses("Body",1.0f);

  qiLogInfo("THWildmyMenue") << "Executing callback method on right bumper event" << std::endl;
  fMemoryProxy.unsubscribeToEvent("RightBumperPressed", getName());
  fMemoryProxy.unsubscribeToEvent("LeftBumperPressed", getName());

  try {

    // make the robot say that the bumper was pressed
    fTtsProxy.say("Rechts wurde gedrückt, Pantomieme wird gestartet.");
    AL::ALProxy pantomimeProxy(getParentBroker(), "THWildPantomime");
    //use random number 0,1,2
    int n = pantomimeProxy.call<int>("getMoveCount");
    int r= std::rand()%n;
    pantomimeProxy.callVoid<int>("makeMove",r);

  }
  catch (const AL::ALError& e) {
    qiLogError("THWildmyMenue") << e.what() << std::endl;
  }
}

/**
 * The callback method being called when the RightBumperPressed event occured.

 */
void THWildmyMenue::onLeftBumperPressed() {
  qiLogInfo("THWildmyMenue") << "Executing callback method on left bumper event" << std::endl;

  fMemoryProxy.unsubscribeToEvent("RightBumperPressed", getName());
  fMemoryProxy.unsubscribeToEvent("LeftBumperPressed", getName());


  try {
    // create a proxy to text to speech
    fTtsProxy = AL::ALTextToSpeechProxy(getParentBroker());

    // make the robot say that the bumper was pressed
    fTtsProxy.say("links wurde gedrückt, Tabu wird gestartet.");

    AL::ALProxy tabuProxy(getParentBroker(), "THWildTabu");
    //use random number 0,1
    int m = tabuProxy.call<int>("getTabuWordsSize");
    int r= std::rand()%m;
    tabuProxy.callVoid<int>("runTabuWord", r);



  }
  catch (const AL::ALError& e) {
    qiLogError("THWildmyMenue") << e.what() << std::endl;
  }
}

/**
 * listen to game finished
 * start main agoin, if game finished

 */

void THWildmyMenue::onGameFinished() {
    qiLogInfo("THWildmyMenue") << "Game Finished!!!" << std::endl;
   // AL::ALMotionProxy motion;
   // motion.setStiffnesses("Body",0.0f);
    //start main agian
    THWildmyMenue::init();

}
