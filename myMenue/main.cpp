/**
 * The main.cpp should not be changed. It is not necessary to understand everything in this file to start working.
 * Just use it like this.
 */

#include <boost/signals2.hpp>
#include <boost-1_55/boost/shared_ptr.hpp>
#include <alcommon/albroker.h>
#include <alcommon/almodule.h>
#include <alcommon/albrokermanager.h>
#include <alcommon/altoolsmain.h>

#include "THWildmyMenue.h"


# ifdef _WIN32
#  define ALCALL __declspec(dllexport)
# else
#  define ALCALL
# endif

extern "C"
{
  ALCALL int _createModule(boost::shared_ptr<AL::ALBroker> pBroker)
  {
    // init broker with the main broker instance
    // from the parent executable
    AL::ALBrokerManager::setInstance(pBroker->fBrokerManager.lock());
    AL::ALBrokerManager::getInstance()->addBroker(pBroker);
      AL::ALModule::createModule<THWildmyMenue>( pBroker, "THWildmyMenue" );

    return 0;
  }

  ALCALL int _closeModule()
  {
    return 0;
  }
}
